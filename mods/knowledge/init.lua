knowledge = {} 


knowledge.recipes = {}

function knowledges_save()
	local file = io.open(minetest.get_worldpath().."/knowledges.txt", "w")
	if file then
		file:write(minetest.serialize(knowledge.recipes))
		file:close()
	end
end

function knowledges_load()
	local file = io.open(minetest.get_worldpath().."/knowledges.txt", "r")
	if file then
		local table = minetest.deserialize(file:read("*all"))
		if type(table) == "table" then
			knowledge.recipes = table
		end
	end
	return {}
end

knowledges_load()

knowledge.set_knowledge = function(player, item)
	local name = player:get_player_name()
	local recipe = item:get_name()
	knowledge.recipes[name] = {"main:stick"}
	table.insert(knowledge.recipes[name], recipe)
	knowledges_save()
end




dofile(minetest.get_modpath("knowledge").."/knowledge.lua")