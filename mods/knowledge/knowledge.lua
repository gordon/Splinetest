learned = "size[4,1;]"
learned = learned .. "button_exit[1.5,0.1;1,2;ok;OK]"
minetest.register_craftitem("knowledge:hatchet", {
	description = "Hatchet recipe",
	inventory_image = "hatchet_recipe.png",
	on_use = function(itemstack, user)
		knowledge.set_knowledge(user, itemstack)
		itemstack:take_item()
		minetest.show_formspec(user:get_player_name(), learned, learned .. "textlist[0,0;3.8,0.5;message;Recipe Hatchet learned]")
		return itemstack
	end,
})
