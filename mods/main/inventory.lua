minetest.register_on_newplayer(function(player)
	player:get_inventory():add_item("main", "knowledge:hatchet")
end)
formspec = "size[8,7.5]" .. "list[current_player;main;0,3.5;8,4;]"
formspec = formspec .. "button[1,0;2,2;tools;Tools]"
formspec = formspec .. "button[3,0;2,2;items;Items]"
formspec = formspec .. "button[5,0;2,2;nodes;Nodes]"
toolsfs = "size[8,7.5]" .. "list[current_player;main;0,3.5;8,4;]"
toolsfs = toolsfs .. "button[1,0;2,2;plantfiber;Plantfiber]"
toolsfs = toolsfs .. "button[5,0;2,2;stick;Stick]"
toolsfs = toolsfs .. "button[5,1;2,2;clay;Clay]"
toolsfs = toolsfs .. "button[1,1;2,2;cobble; Cobble]"
toolsfs = toolsfs .. "button[1,2;2,2;pebble; Pebble]"
toolsfs = toolsfs .. "button[-0.2,-0.8;1.5,2;back;Back]"
minetest.register_on_joinplayer(function(player)
	player:set_inventory_formspec(formspec)
end)
local timer = 0
minetest.register_globalstep(function(dtime)
	timer = timer + dtime;
	if timer >= 1 then
		for _,player in ipairs(minetest.get_connected_players()) do
			local name = player:get_player_name()
			if knowledge.recipes[name] == nil then
				knowledge.recipes[name] = {"main:stick"}
			end
			local arr = knowledge.recipes[name]
			for k, v in pairs(arr) do
				if v == "knowledge:hatchet" then
					toolsfs = toolsfs .. "button[5,2;2,2;hatchet;Hatchet]"
					break
				end
			end
		end
		timer = 0
	end
end)



minetest.register_on_player_receive_fields(function(clicker, formname, fields)
					if fields.tools then
						clicker:set_inventory_formspec(toolsfs)
					end
					if fields.back then
						clicker:set_inventory_formspec(formspec)
					end
					if fields.plantfiber then
						if clicker:get_inventory():contains_item("main", "main:grass") then
							clicker:get_inventory():remove_item("main", "main:grass")
							clicker:get_inventory():add_item("main", "main:plantfiber")
						end
					end
					if fields.stick then
						if clicker:get_inventory():contains_item("main", "main:branch") then
							clicker:get_inventory():remove_item("main", "main:branch")
							clicker:get_inventory():add_item("main", "main:stick")
						end
					end
					if fields.hatchet then
						if clicker:get_inventory():contains_item("main", "main:plantfiber") and 
							clicker:get_inventory():contains_item("main", "main:pebble") and
							clicker:get_inventory():contains_item("main", "main:stick") then
								clicker:get_inventory():remove_item("main", "main:plantfiber")
								clicker:get_inventory():remove_item("main", "main:pebble")
								clicker:get_inventory():remove_item("main", "main:stick")
								clicker:get_inventory():add_item("main", "tools:hatchet")
						end
					end
					if fields.clay then
						if clicker:get_inventory():contains_item("main", "main:clay_lump 9") then
							clicker:get_inventory():remove_item("main", "main:clay_lump 9")
							clicker:get_inventory():add_item("main", "main:clay")
						end
					end
					if fields.cobble then
						if clicker:get_inventory():contains_item("main", "main:pebble 9") then
							clicker:get_inventory():remove_item("main", "main:pebble 9")
							clicker:get_inventory():add_item("main", "main:cobble")
						end
					end
					if fields.pebble then
						if clicker:get_inventory():contains_item("main", "main:cobble") then
							clicker:get_inventory():remove_item("main", "main:cobble")
							clicker:get_inventory():add_item("main", "main:pebble 9")
						end
					end 
	end)