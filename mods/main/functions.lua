-- mods/main/functions.lua

--
-- Sounds
--

function main.node_sound_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "", gain = 1.0}
	table.dug = table.dug or
			{name = "main_dug_node", gain = 0.25}
	table.place = table.place or
			{name = "main_place_node_hard", gain = 1.0}
	return table
end

function main.node_sound_stone_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_hard_footstep", gain = 0.5}
	table.dug = table.dug or
			{name = "main_hard_footstep", gain = 1.0}
	main.node_sound_mains(table)
	return table
end

function main.node_sound_dirt_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_dirt_footstep", gain = 1.0}
	table.dug = table.dug or
			{name = "main_dirt_footstep", gain = 1.5}
	table.place = table.place or
			{name = "main_place_node", gain = 1.0}
	main.node_sound_mains(table)
	return table
end

function main.node_sound_sand_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_sand_footstep", gain = 0.2}
	table.dug = table.dug or
			{name = "main_sand_footstep", gain = 0.4}
	table.place = table.place or
			{name = "main_place_node", gain = 1.0}
	main.node_sound_mains(table)
	return table
end

function main.node_sound_wood_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_wood_footstep", gain = 0.5}
	table.dug = table.dug or
			{name = "main_wood_footstep", gain = 1.0}
	main.node_sound_mains(table)
	return table
end

function main.node_sound_leaves_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_grass_footstep", gain = 0.35}
	table.dug = table.dug or
			{name = "main_grass_footstep", gain = 0.7}
	table.dig = table.dig or
			{name = "main_dig_crumbly", gain = 0.4}
	table.place = table.place or
			{name = "main_place_node", gain = 1.0}
	main.node_sound_mains(table)
	return table
end

function main.node_sound_glass_mains(table)
	table = table or {}
	table.footstep = table.footstep or
			{name = "main_glass_footstep", gain = 0.5}
	table.dug = table.dug or
			{name = "main_break_glass", gain = 1.0}
	main.node_sound_mains(table)
	return table
end


--
-- Lavacooling
--

main.cool_lava_source = function(pos)
	minetest.set_node(pos, {name = "main:obsidian"})
	minetest.sound_play("main_cool_lava", {pos = pos, gain = 0.25})
end

main.cool_lava_flowing = function(pos)
	minetest.set_node(pos, {name = "main:stone"})
	minetest.sound_play("main_cool_lava", {pos = pos, gain = 0.25})
end

minetest.register_abm({
	nodenames = {"main:lava_flowing"},
	neighbors = {"group:water"},
	interval = 1,
	chance = 1,
	action = function(...)
		main.cool_lava_flowing(...)
	end,
})

minetest.register_abm({
	nodenames = {"main:lava_source"},
	neighbors = {"group:water"},
	interval = 1,
	chance = 1,
	action = function(...)
		main.cool_lava_source(...)
	end,
})


--
-- Papyrus and cactus growing
--

-- wrapping the functions in abm action is necessary to make overriding them possible

function main.grow_cactus(pos, node)
	if node.param2 >= 4 then
		return
	end
	pos.y = pos.y - 1
	if minetest.get_item_group(minetest.get_node(pos).name, "sand") == 0 then
		return
	end
	pos.y = pos.y + 1
	local height = 0
	while node.name == "main:cactus" and height < 4 do
		height = height + 1
		pos.y = pos.y + 1
		node = minetest.get_node(pos)
	end
	if height == 4 or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name = "main:cactus"})
	return true
end

function main.grow_papyrus(pos, node)
	pos.y = pos.y - 1
	local name = minetest.get_node(pos).name
	if name ~= "main:dirt_with_grass" and name ~= "main:dirt" then
		return
	end
	if not minetest.find_node_near(pos, 3, {"group:water"}) then
		return
	end
	pos.y = pos.y + 1
	local height = 0
	while node.name == "main:papyrus" and height < 4 do
		height = height + 1
		pos.y = pos.y + 1
		node = minetest.get_node(pos)
	end
	if height == 4 or node.name ~= "air" then
		return
	end
	minetest.set_node(pos, {name = "main:papyrus"})
	return true
end

minetest.register_abm({
	nodenames = {"main:cactus"},
	neighbors = {"group:sand"},
	interval = 50,
	chance = 20,
	action = function(...)
		main.grow_cactus(...)
	end
})

minetest.register_abm({
	nodenames = {"main:papyrus"},
	neighbors = {"main:dirt", "main:dirt_with_grass"},
	interval = 50,
	chance = 20,
	action = function(...)
		main.grow_papyrus(...)
	end
})


--
-- dig upwards
--

function main.dig_up(pos, node, digger)
	if digger == nil then return end
	local np = {x = pos.x, y = pos.y + 1, z = pos.z}
	local nn = minetest.get_node(np)
	if nn.name == node.name then
		minetest.node_dig(np, nn, digger)
	end
end


--
-- Leafdecay
--

main.leafdecay_trunk_cache = {}
main.leafdecay_enable_cache = true
-- Spread the load of finding trunks
main.leafdecay_trunk_find_allow_accumulator = 0

minetest.register_globalstep(function(dtime)
	local finds_per_second = 5000
	main.leafdecay_trunk_find_allow_accumulator =
			math.floor(dtime * finds_per_second)
end)

main.after_place_leaves = function(pos, placer, itemstack, pointed_thing)
	local node = minetest.get_node(pos)
	node.param2 = 1
	minetest.set_node(pos, node)
end

minetest.register_abm({
	nodenames = {"group:leafdecay"},
	neighbors = {"air", "group:liquid"},
	-- A low interval and a high inverse chance spreads the load
	interval = 2,
	chance = 5,

	action = function(p0, node, _, _)
		--print("leafdecay ABM at "..p0.x..", "..p0.y..", "..p0.z..")")
		local do_preserve = false
		local d = minetest.registered_nodes[node.name].groups.leafdecay
		if not d or d == 0 then
			--print("not groups.leafdecay")
			return
		end
		local n0 = minetest.get_node(p0)
		if n0.param2 ~= 0 then
			--print("param2 ~= 0")
			return
		end
		local p0_hash = nil
		if main.leafdecay_enable_cache then
			p0_hash = minetest.hash_node_position(p0)
			local trunkp = main.leafdecay_trunk_cache[p0_hash]
			if trunkp then
				local n = minetest.get_node(trunkp)
				local reg = minetest.registered_nodes[n.name]
				-- Assume ignore is a trunk, to make the thing
				-- work at the border of the active area
				if n.name == "ignore" or (reg and reg.groups.tree and
						reg.groups.tree ~= 0) then
					--print("cached trunk still exists")
					return
				end
				--print("cached trunk is invalid")
				-- Cache is invalid
				table.remove(main.leafdecay_trunk_cache, p0_hash)
			end
		end
		if main.leafdecay_trunk_find_allow_accumulator <= 0 then
			return
		end
		main.leafdecay_trunk_find_allow_accumulator =
				main.leafdecay_trunk_find_allow_accumulator - 1
		-- Assume ignore is a trunk, to make the thing
		-- work at the border of the active area
		local p1 = minetest.find_node_near(p0, d, {"ignore", "group:tree"})
		if p1 then
			do_preserve = true
			if main.leafdecay_enable_cache then
				--print("caching trunk")
				-- Cache the trunk
				main.leafdecay_trunk_cache[p0_hash] = p1
			end
		end
		if not do_preserve then
			-- Drop stuff other than the node itself
			local itemstacks = minetest.get_node_drops(n0.name)
			for _, itemname in ipairs(itemstacks) do
				if minetest.get_item_group(n0.name, "leafdecay_drop") ~= 0 or
						itemname ~= n0.name then
					local p_drop = {
						x = p0.x - 0.5 + math.random(),
						y = p0.y - 0.5 + math.random(),
						z = p0.z - 0.5 + math.random(),
					}
					minetest.add_item(p_drop, itemname)
				end
			end
			-- Remove node
			minetest.remove_node(p0)
			nodeupdate(p0)
		end
	end
})


--
-- Grass growing
--

minetest.register_abm({
	nodenames = {"main:dirt"},
	interval = 2,
	chance = 200,
	action = function(pos, node)
		local above = {x = pos.x, y = pos.y + 1, z = pos.z}
		local name = minetest.get_node(above).name
		local nodedef = minetest.registered_nodes[name]
		if nodedef and (nodedef.sunlight_propagates or nodedef.paramtype == "light") and
				nodedef.liquidtype == "none" and
				(minetest.get_node_light(above) or 0) >= 13 then
			if name == "main:snow" or name == "main:snowblock" then
				minetest.set_node(pos, {name = "main:dirt_with_snow"})
			else
				minetest.set_node(pos, {name = "main:dirt_with_grass"})
			end
		end
	end
})

minetest.register_abm({
	nodenames = {"main:dirt_with_grass"},
	interval = 2,
	chance = 20,
	action = function(pos, node)
		local above = {x = pos.x, y = pos.y + 1, z = pos.z}
		local name = minetest.get_node(above).name
		local nodedef = minetest.registered_nodes[name]
		if name ~= "ignore" and nodedef and not ((nodedef.sunlight_propagates or
				nodedef.paramtype == "light") and
				nodedef.liquidtype == "none") then
			minetest.set_node(pos, {name = "main:dirt"})
		end
	end
})

